﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Voyageur.Models
{
    public class ShareModels
    {

        [Key]
        public int ID { get; set; }
        [Display(Name = "Resim")]
        public string Image { get; set; }
        [Display(Name = "Mekan Adı")]
        public string Name { get; set; }
        [Display(Name = "Adres")]
        public string Address { get; set; }
        [Display(Name = "Yorum")]
        public string Comment { get; set; }
        [Display(Name = "Puan")]
        public string Point { get; set; }
        [Display(Name = "Kategori")]
        public string Kategori { get; set; }

    }
}