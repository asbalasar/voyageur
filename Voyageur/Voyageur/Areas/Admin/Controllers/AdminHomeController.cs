﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Voyageur.Models;

namespace Voyageur.Areas.Admin.Controllers
{
    [Authorize]
    public class AdminHomeController : Controller
    {

        ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;


        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Admin/Home
        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }
        public ActionResult RolAta()
        {
            return View();

        }

        [HttpPost]
        public async Task<ActionResult> RolAta(String id)
        {
            var user = await UserManager.FindByIdAsync(id);
            var userStore = new UserStore<ApplicationUser>(db);
            var userManager = new UserManager<ApplicationUser>(userStore);
            if (user.Email == "mustafa.glr97@gmail.com")
            {
                userManager.AddToRole(user.Id, "Admin");
            }
            else
            {
                userManager.AddToRole(user.Id, "Uye");
            }
            return RedirectToAction("Index");

        }



    }

}
